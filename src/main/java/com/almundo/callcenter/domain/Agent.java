package com.almundo.callcenter.domain;

import com.almundo.callcenter.enums.EmployeeType;
import com.almundo.callcenter.enums.StatusAgent;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Javier Bernal
 */
@Table
@Entity
@Getter
@Setter
public class Agent implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String document;

    @Enumerated(EnumType.ORDINAL)
    private EmployeeType type;

    @Enumerated(EnumType.STRING)
    private StatusAgent status;

}
