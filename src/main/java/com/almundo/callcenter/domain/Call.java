package com.almundo.callcenter.domain;

import com.almundo.callcenter.enums.Status;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Javier Bernal
 */
@Table
@Entity
@Getter
@Setter
public class Call implements Serializable {

    @Id
    @GeneratedValue
    private long id;

    private long duration;

    @Enumerated
    private Status status;

    @ManyToOne(optional = true, fetch = FetchType.EAGER)
    private Agent agent;

}
