package com.almundo.callcenter.utilities;

/**
 *
 * @author Javier Bernal
 */
public class GeneralConstants {

    public static final String BASE_PACKAGE = "com.almundo.callcenter";

    public static final int CAPACITY = 10;

    public static final int MAX_DURATION = 10;

    public static final int MIN_DURATION = 5;
}
