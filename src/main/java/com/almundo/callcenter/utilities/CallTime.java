package com.almundo.callcenter.utilities;

import static com.almundo.callcenter.utilities.GeneralConstants.MAX_DURATION;
import static com.almundo.callcenter.utilities.GeneralConstants.MIN_DURATION;
import java.util.Random;

/**
 *
 * @author Javier Bernal
 */
public class CallTime {

    public static int getDurationCall() {
        Random rand = new Random();
        int timeCall = rand.nextInt((MAX_DURATION - MIN_DURATION) + 1) + MIN_DURATION;

        return timeCall * 1000;
    }

}
