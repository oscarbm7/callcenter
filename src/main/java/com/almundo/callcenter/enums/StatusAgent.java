package com.almundo.callcenter.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * @author Javier Bernal
 */
@AllArgsConstructor
@Getter
public enum StatusAgent {
    FREE,
    ONCALL;
}
