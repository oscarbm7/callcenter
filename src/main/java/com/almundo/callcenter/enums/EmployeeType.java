package com.almundo.callcenter.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * @author Javier Bernal
 */
@AllArgsConstructor
@Getter
public enum EmployeeType {
    OPERATOR(0),
    SUPERVISOR(1),
    DIRECTOR(2);

    private int value;
}
