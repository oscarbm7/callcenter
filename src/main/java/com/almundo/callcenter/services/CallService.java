package com.almundo.callcenter.services;

import com.almundo.callcenter.domain.Agent;
import com.almundo.callcenter.domain.Call;
import static com.almundo.callcenter.enums.Status.ANSWERED;
import static com.almundo.callcenter.enums.Status.TAKEN;
import static com.almundo.callcenter.enums.Status.WAITING;
import static com.almundo.callcenter.enums.StatusAgent.FREE;
import static com.almundo.callcenter.enums.StatusAgent.ONCALL;
import com.almundo.callcenter.repositories.AgentRepository;
import com.almundo.callcenter.repositories.CallRepository;
import com.almundo.callcenter.utilities.CallTime;
import java.util.List;
import java.util.concurrent.Future;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

/**
 *
 * @author Javier Bernal
 */
@Slf4j
@Service
public class CallService {

    @Autowired
    CallRepository callRepository;

    @Autowired
    AgentRepository AgentRepository;

    /**
     * service for answer call 
     *
     * @param inputCall call object
     * @return future object with call generated.
     */
    @Async
    public Future<Call> answerCall(Call inputCall) {

        Call call = assignCall(inputCall);
        try {

            if (call.getAgent() != null) {
                call.setDuration(call.getDuration() == 0
                        ? CallTime.getDurationCall() : call.getDuration());
                call.setStatus(ANSWERED);
                Thread.sleep(call.getDuration());
                call = callRepository.save(call);
                call.getAgent().setStatus(FREE);
                AgentRepository.save(call.getAgent());

                log.info("Call " + call.getId() + " taken.. Duration:" + call.getDuration());
            }

        } catch (InterruptedException ex) {
            log.error("Error Async answerCall:", ex);
        }
        return new AsyncResult<>(call);
    }

    /**
     * method for to assign call
     *
     * @param inputCall call object
     * @return call object generated.
     */    
    private Call assignCall(Call inputCall) {

        Call call = (inputCall == null ? new Call() : inputCall);

        List<Agent> agentsList = AgentRepository.searchAvailable(ONCALL);

        if (!agentsList.isEmpty()) {
            Agent agent = agentsList.get(0);

            if (agent.getDocument() != null) {
                call.setAgent(agent);
                call.setStatus(TAKEN);
                agent.setStatus(ONCALL);
                AgentRepository.save(agent);
                log.info("Call Asign to " + agent.getId() + " - " + agent.getName());
            }
        } else {
            call.setStatus(WAITING);
            log.info("Agent Unavailable.... Call in queue ");
        }
        call = callRepository.save(call);
        return call;
    }

}
