package com.almundo.callcenter;

import static com.almundo.callcenter.utilities.GeneralConstants.BASE_PACKAGE;
import static com.almundo.callcenter.utilities.GeneralConstants.CAPACITY;
import java.util.concurrent.Executor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@SpringBootApplication
@EnableAutoConfiguration
@EnableJpaRepositories(basePackages = BASE_PACKAGE)
@EntityScan(basePackages = BASE_PACKAGE)
@ComponentScan(basePackages = BASE_PACKAGE)
@EnableScheduling
@EnableAsync
public class AlmundoApplication {

    public static void main(String[] args) {
        SpringApplication.run(AlmundoApplication.class, args);
    }

    /**
     * Configure thread pool
     *
     * @return executor.
     */
    @Bean
    public Executor asyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();

        int capacity = CAPACITY;
        executor.setCorePoolSize(capacity);
        executor.setMaxPoolSize(capacity);

        executor.setThreadNamePrefix("CallThread-");
        executor.initialize();
        return executor;
    }

}
