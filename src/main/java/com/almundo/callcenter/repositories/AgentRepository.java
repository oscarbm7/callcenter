package com.almundo.callcenter.repositories;

import com.almundo.callcenter.domain.Agent;
import com.almundo.callcenter.enums.StatusAgent;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Javier Bernal
 */
@Repository
public interface AgentRepository extends JpaRepository<Agent, Long> {

    @Query("SELECT agen "
            + "FROM Agent agen "
            + "WHERE agen.status != :status "
            + "ORDER BY agen.type ASC")
    List<Agent> searchAvailable(@Param(value = "status") StatusAgent status);
}
