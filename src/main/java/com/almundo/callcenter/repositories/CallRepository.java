package com.almundo.callcenter.repositories;

import com.almundo.callcenter.domain.Call;
import com.almundo.callcenter.enums.Status;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Javier Bernal
 */
@Repository
public interface CallRepository extends JpaRepository<Call, Long> {

    List<Call> findByAgentIsNullAndStatus(Status status);

    Call save(Call call);
}
