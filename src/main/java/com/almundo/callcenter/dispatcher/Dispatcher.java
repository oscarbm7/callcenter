package com.almundo.callcenter.dispatcher;

import com.almundo.callcenter.services.CallService;
import java.util.concurrent.RejectedExecutionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Javier Bernal
 */
@Slf4j
@RestController
public class Dispatcher {

    @Autowired
    CallService callService;

    /**
     * REST service for to dispatch call
     *
     * @return string message.
     */
    @RequestMapping(value = "/call", method = RequestMethod.GET)
    public String dispatchCall() {

        try {
            log.info("Iniciando llamada.... ");

            callService.answerCall(null);

            return "Call in process...";
        } catch (RejectedExecutionException ex) {
            return null;
        }

    }

}
