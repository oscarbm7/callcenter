package com.almundo.callcenter.jobs;

import com.almundo.callcenter.domain.Call;
import static com.almundo.callcenter.enums.Status.WAITING;
import com.almundo.callcenter.repositories.CallRepository;
import com.almundo.callcenter.services.CallService;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author Javier Bernal
 */
@Slf4j
@Component
public class CronTask {

    @Autowired
    CallRepository callRepository;

    @Autowired
    CallService callService;

    /**
     * Job process for to review waiting calls.
     *
     */
    @Scheduled(fixedRateString = "${com.almundo.process.fixedRate}")
    public void processQueue() {
        try {
            List<Call> calls = callRepository.findByAgentIsNullAndStatus(WAITING);
            log.info("Start Process Queue.....CALLS IN QUEUE: " + calls.size());
            calls.forEach((call) -> {
                log.info("Process call in queue ");
                callService.answerCall(call);
            });
        } catch (Exception ex) {
            log.error("Error job answerCall:", ex);
        }

    }

}
