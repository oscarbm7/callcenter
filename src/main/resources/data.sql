insert into Agent(id, name, document, type, status)
values(1,'Javier Bernal', 'E1234567', 0, 'FREE');

insert into Agent(id, name, document, type, status)
values(2,'Oscar Mateus', 'E1234567', 0, 'FREE');

insert into Agent(id, name, document, type, status)
values(3,'William Bernal', 'E1234567', 0, 'FREE');

insert into Agent(id, name, document, type, status)
values(4,'Carlos Mateus', 'E1234567', 0, 'FREE');

insert into Agent(id, name, document, type, status)
values(5,'Diana Bernal', 'E1234567', 0, 'FREE');

insert into Agent(id, name, document, type, status)
values(6,'Daniel Ortiz', 'A1234568', 1, 'FREE');

insert into Agent(id, name, document, type, status)
values(7,'Cesar Ortiz', 'A1234568', 1, 'FREE');

insert into Agent(id, name, document, type, status)
values(8,'Carlos Rodriguez', 'A1234666', 2, 'FREE');