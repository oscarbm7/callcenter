package com.almundo.callcenter;

import com.almundo.callcenter.service.CallServiceTest;
import com.almundo.callcenter.service.CallServiceTestDB;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    CallServiceTestDB.class,
    CallServiceTest.class
})
public class AlmundoApplicationTests {

}
