package com.almundo.callcenter.mocks;

import com.almundo.callcenter.domain.Call;

public class CallMock {
    
    public static Call get(){
        Call call = new Call();
        call.setDuration(10000);
        return call;
    }
}
