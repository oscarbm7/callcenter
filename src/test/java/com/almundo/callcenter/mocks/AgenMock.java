package com.almundo.callcenter.mocks;

import com.almundo.callcenter.domain.Agent;
import com.almundo.callcenter.enums.EmployeeType;
import com.almundo.callcenter.enums.StatusAgent;

public class AgenMock {
    
    public static Agent get(EmployeeType type){
        Agent agent = new Agent();
        agent.setId(new Long(1));
        agent.setDocument("E1234567");
        agent.setName("Javier Bernal");
        agent.setType(type);
        agent.setStatus(StatusAgent.FREE);
        return agent;
    }
}
