package com.almundo.callcenter.service;

import com.almundo.callcenter.domain.Agent;
import com.almundo.callcenter.domain.Call;
import static com.almundo.callcenter.enums.EmployeeType.OPERATOR;
import static com.almundo.callcenter.enums.EmployeeType.SUPERVISOR;
import static com.almundo.callcenter.enums.Status.ANSWERED;
import static com.almundo.callcenter.enums.StatusAgent.ONCALL;
import com.almundo.callcenter.mocks.AgenMock;
import com.almundo.callcenter.mocks.CallMock;
import com.almundo.callcenter.repositories.AgentRepository;
import com.almundo.callcenter.repositories.CallRepository;
import com.almundo.callcenter.services.CallService;
import static com.almundo.callcenter.utilities.GeneralConstants.BASE_PACKAGE;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.BDDMockito.given;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@EnableAsync
@SpringBootTest(classes = {
    CallService.class,
    CallRepository.class,
    AgentRepository.class
})
public class CallServiceTest {

    @MockBean
    AgentRepository employeeRepository;

    @MockBean
    CallRepository callRepository;

    @Autowired
    CallService callService;

    Call call;

    Agent agent;

    List<Agent> agentList = new ArrayList<>();

    @Before
    public void setup() throws Exception {
        call = CallMock.get();
        agent = AgenMock.get(OPERATOR);

        agentList.add(agent);
        agentList.add(AgenMock.get(SUPERVISOR));

        given(employeeRepository.searchAvailable(ONCALL))
                .willReturn(agentList);

        given(employeeRepository.save(agent))
                .willReturn(agent);

        given(callRepository.save(call))
                .willReturn(call);

    }

    @Test
    public void callInProgress() throws InterruptedException, ExecutionException {
            assertThat(callService.answerCall(call).get().getStatus()
                    .toString()).isEqualTo(ANSWERED.toString());
    }
    
}
