package com.almundo.callcenter.service;

import com.almundo.callcenter.domain.Call;
import static com.almundo.callcenter.enums.Status.ANSWERED;
import com.almundo.callcenter.repositories.AgentRepository;
import com.almundo.callcenter.repositories.CallRepository;
import com.almundo.callcenter.services.CallService;
import static com.almundo.callcenter.utilities.GeneralConstants.BASE_PACKAGE;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.IntStream;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@EnableAsync
@EntityScan(basePackages = BASE_PACKAGE)
@EnableJpaRepositories(basePackages = BASE_PACKAGE)
@SpringBootTest(classes = {
    CallService.class,
    CallRepository.class,
    AgentRepository.class
})
public class CallServiceTestAsincDB {

    @Autowired
    CallService callService;

    @Test
    public void aswerCall() throws InterruptedException, ExecutionException {
        for (int index : IntStream.range(0, 10).toArray()) {
            
            assertThat(callService.answerCall(null).get().getStatus()
                    .toString()).isEqualTo(ANSWERED.toString());
            //callService.answerCall(null);
        }
        
    }
}
